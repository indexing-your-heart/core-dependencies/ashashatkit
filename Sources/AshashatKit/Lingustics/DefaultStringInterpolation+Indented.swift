//
//  DefaultStringInterpolation+Indented.swift
//  Indexing Your Heart
//
//  Created by Marquis Kurt on 9/24/23.
//
//  This Source Code Form is subject to the terms of the Mozilla Public
//  License, v. 2.0. If a copy of the MPL was not distributed with this
//  file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

import Foundation

// Original: https://gist.github.com/AnthonyMDev/08101004927a0e132f47875e36199121
// Thanks @AnthonyMDev
extension DefaultStringInterpolation {
    mutating func appendInterpolation<T>(indented string: T) {
        // swiftlint:disable:next compiler_protocol_init
        let indent = String(stringInterpolation: self).reversed().prefix { " \t".contains($0) }
        let root = String(describing: string)
        appendLiteral(
            indent.isEmpty ? root: root.split(separator: "\n", omittingEmptySubsequences: false)
                .joined(separator: "\n" + indent)
        )
    }
}
