//
//  AshashatGrammaticalPersonModifier.swift
//  Indexing Your Heart
//
//  Created by Marquis Kurt on 9/20/23.
//
//  This Source Code Form is subject to the terms of the Mozilla Public
//  License, v. 2.0. If a copy of the MPL was not distributed with this
//  file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

import ConlangKit
import Foundation

/// A modifier describing the grammatical person associated with the word.
public enum AshashatGrammaticalPersonModifier: AshashatModifier {
    /// The first person (i.e., the speaker).
    case first

    /// The second person (i.e., the recipient or other speaker).
    case second

    /// The third person.
    case third

    public var word: some LinguisticRepresentable {
        switch self {
        case .first:
            Morpheme(stringLiteral: "[ba]")
        case .second:
            Morpheme(stringLiteral: "[bi]")
        case .third:
            Morpheme(stringLiteral: "[bu]")
        }
    }
}

/// An [ʔaʃaʃat] word with an attached grammatical person.
///
/// This can only be constructed using the ``AshashatWord/grammaticalPerson(_:)`` modifier.
public struct GrammaticalPersonAshashatWord<Reference: AshashatWord>: AshashatWord {
    /// The grammatical person associated with this word.
    var person: AshashatGrammaticalPersonModifier

    /// The reference word for which the grammatical person is assigned to.
    var reference: Reference

    internal init(person: AshashatGrammaticalPersonModifier, reference: Reference) {
        self.person = person
        self.reference = reference
    }

    public var word: some LinguisticRepresentable {
        reference.word
            .prefixed(by: person.word as! Reference.Word.BoundMorpheme, // swiftlint:disable:this force_cast
                      repairingWith: .ashashat)
    }
}

extension GrammaticalPersonAshashatWord: CustomStringConvertible {
    public var description: String {
       """
       ▿ PrefixedAshashatWord
        - Person: \(person)
        ▿ Reference: \(Reference.self)
          \(indented: reference)
       """
    }
}

public extension AshashatModifier {
    /// Specifies an associated grammatical person.
    ///
    /// Grammtical person modifiers are applied as prefixes, typically for modifiers. For example, the following
    /// produces "your idea":
    ///
    /// ```swift
    /// var word: some AshashatWord {
    ///     AshashatPrimitive.idea
    ///         .owning { word in
    ///             word.grammaticalPerson(.first)
    ///         } // produces [biʔaʃak'asu:p]
    /// }
    /// ```
    ///
    /// - Parameter person: The grammatical person to associate with the word.
    func grammaticalPerson(_ person: AshashatGrammaticalPersonModifier) -> some AshashatWord {
        GrammaticalPersonAshashatWord(person: person, reference: self)
    }
}
