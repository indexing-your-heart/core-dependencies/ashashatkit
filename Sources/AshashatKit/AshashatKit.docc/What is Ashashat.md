# What is [ʔaʃaʃat]?

@Metadata {
    @PageColor(purple)
    @CallToAction(purpose: link, url: "https://marquiskurt.itch.io/ashashat-lang")
}

Learn the fundamental aspects that make up the [ʔaʃaʃat] conlang.

## Phonology

The phonological inventory of [ʔaʃaʃat] has been designed to keep the most common consonant and vowel sounds, while
adding a few rare or unusual sounds to offer flavor. The language is not tone-based, focusing more on the construction
of words and phrases to convey meaning.

[ʔaʃaʃat] offers four vowels: [a], [e], [i], and [u]. These vowels were chosen for their distinct sounds which are
easier to identify.

Additionally, the language offers a limited set of consonants: [ʔ], [k], [k'], [t], [s], [b], [p], [l], [n], and [ʃ].
Given how similar [b] and [p] are in pronunciation (as one is voiced and the other voiceless), assimilation does occur
when words are written with these sounds.

## Writing System

The official writing system for [ʔaʃaʃat] is a reverse abugida, meaning that the vowels are written full-height, and
additional consonants are written as diacritics to the vowels. This approach was taken since reverse abugidas are less
common in languages across the world; additionally, readers and speakers of the language have less symbols to learn
since the consonants modify the vowels.

Below is a diagram of the writing system, showing off the various symbols and their placements. The vowels are modeled
after the shape of the mouth when pronouncing the vowel sounds. Likewise, the consonants and their placement on a vowel
as a diacritic are relative to their positions on the IPA chart, with the exception to the ejective k [k'].

![The \[ʔaʃaʃat\] writing system](writing.png)

Words written in the [ʔaʃaʃat] writing system are read from left to right. However, the construction of words start at
the center with the base, and the modifiers are attached outwards on either side.

The writing system includes other symbols to denote the end of a sentence (akin to a period or full stop in English),
expressing that something may be possible and/or permitted (i.e., a “merged” can/may), and to indicate passive voice.
Additionally, a repeater character exists to elongate the vowel sound before it, and the intensified duplicant character
reduplicates the syllable preceding it with slight modification.
