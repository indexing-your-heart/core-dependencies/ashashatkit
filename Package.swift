// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "AshashatKit",
    platforms: [.macOS(.v10_15), .iOS(.v13)],
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "AshashatKit",
            targets: ["AshashatKit"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/Indexing-Your-Heart/core-dependencies/ConlangKit",
                 from: "1.0.0-DEVELOPMENT-SNAPSHOT-2023-10-03-a")
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .target(
            name: "AshashatKit",
            dependencies: ["ConlangKit"]),
        .testTarget(
            name: "AshashatKitTests",
            dependencies: ["AshashatKit", "ConlangKit"]),
    ]
)
