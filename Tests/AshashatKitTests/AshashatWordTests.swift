//
//  AshashatWordTests.swift
//  Indexing Your Heart
//
//  Created by Marquis Kurt on 9/19/23.
//
//  This Source Code Form is subject to the terms of the Mozilla Public
//  License, v. 2.0. If a copy of the MPL was not distributed with this
//  file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

@testable import AshashatKit
import Foundation
import XCTest

final class AshashatWordTests: XCTestCase {
    func testStringInitialization() throws {
        let person = AshashatPrimitive.person
        let word = String(ashashatWord: person)
        XCTAssertEqual(word, "[pubaʃ]")
    }
}
