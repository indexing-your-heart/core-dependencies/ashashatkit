//
//  AshashatModifierTests.swift
//  Indexing Your Heart
//
//  Created by Marquis Kurt on 9/20/23.
//
//  This Source Code Form is subject to the terms of the Mozilla Public
//  License, v. 2.0. If a copy of the MPL was not distributed with this
//  file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

@testable import AshashatKit
import Foundation
import XCTest

final class AshashatModifierTests: XCTestCase {
    func testPluralizationModifier() throws {
        let someThings: some AshashatWord = {
            AshashatPrimitive.thing
                .pluralized(.some)
        }()

        XCTAssertTrue(someThings is PluralizedAshashatWord<AshashatPrimitive>)
        XCTAssertEqual(String(ashashatWord: someThings), "[iʔaʃaʃatasa]")

        let nothing: some AshashatWord = {
            AshashatPrimitive.thing
                .pluralized(.none)
        }()

        XCTAssertTrue(nothing is PluralizedAshashatWord<AshashatPrimitive>)
        XCTAssertEqual(String(ashashatWord: nothing), "[i:ʔaʃaʃat]")
    }

    func testPossessionModifier() throws {
        let word: some AshashatWord = {
            AshashatPrimitive.animal
                .owning()
        }()
        XCTAssertTrue(word is PossessedAshashatWord<AshashatPrimitive>)
        XCTAssertEqual(String(ashashatWord: word), "[k'abupeʃasu:p]")
    }

    func testGrammaticalPersonModifier() throws {
        let yourIdea: some AshashatWord = {
            AshashatPrimitive.idea
                .owning { word in
                    word.grammaticalPerson(.second)
                }
        }()
        XCTAssertTrue(
            yourIdea is
            CircumfixedAshashatWord<AshashatPrimitive, GrammaticalPersonAshashatWord<PossessionAshashatWord>>)
        XCTAssertEqual(String(ashashatWord: yourIdea), "[biʔaʃak'asu:p]")
    }

    func testActionModifier() throws {
        let word: some AshashatWord = {
            AshashatPrimitive.idea
                .action(.speakable)
        }()
        XCTAssertTrue(word is ActionableAshashatWord<AshashatPrimitive>)
        XCTAssertEqual(String(ashashatWord: word), "[ʔaʃakasu]")
    }

    func testLogicalConjunctionModifier() throws {
        let unmarkableIdea: some AshashatWord = {
            AshashatPrimitive.idea
                .action(.markable) { action in
                    action.logicalConjunction(using: .not)
                }
        }()
        XCTAssertTrue(
            unmarkableIdea is SuffixedAshashatWord<AshashatPrimitive, LogicalAshashatWord<AshashatActionModifier>>)
        XCTAssertEqual(String(ashashatWord: unmarkableIdea), "[ʔaʃabasukaʔabin]")
    }

    func testScientificDomainModifier() throws {
        let iPad: some AshashatWord = {
            AshashatShape.slab
                .scientificDomain(.electrical)
        }()
        XCTAssertTrue(iPad is ScientificDomainAshashatWord<AshashatShape>)
        XCTAssertEqual(String(ashashatWord: iPad), "[esiʃaʃaku]")
    }

    func testScaleModifier() throws {
        let bigBall: some AshashatWord = {
            AshashatShape.sphere
                .scaled(to: .large)
        }()
        XCTAssertTrue(bigBall is ScaledAshashatWord<AshashatShape>)
        XCTAssertEqual(String(ashashatWord: bigBall), "[ʔilinaʃi]")

        let longSlab: some AshashatWord = {
            AshashatShape.slab
                .scaled(to: .medium, axis: .length)
        }()
        XCTAssertTrue(longSlab is ScaledAshashatWord<AshashatShape>)
        XCTAssertEqual(String(ashashatWord: longSlab), "[siʃaʃek'aʃa]")
    }

    func testSenseModifier() throws {
        let art: some AshashatWord = {
            AshashatPrimitive.idea
                .sense(.visual)
        }()
        XCTAssertTrue(art is SenseAshashatWord<AshashatPrimitive>)
        XCTAssertEqual(String(ashashatWord: art), "[iʔiʔaʃ]")
    }

    func testColorModifier() throws {
        let apple: some AshashatWord = {
            AshashatShape.sphere
                .sense(.tastable)
                .color(.red)
        }()
        XCTAssertTrue(apple is ColorizedAshashatWord<SenseAshashatWord<AshashatShape>>)
        XCTAssertEqual(String(ashashatWord: apple), "[tatauʔuʔilin]")
    }

    func testSpeedModifier() throws {
        let fastBall: some AshashatWord = {
            AshashatShape.sphere
                .speed(.fast)
        }()
        XCTAssertTrue(fastBall is MovingAshashatWord<AshashatShape>)
        XCTAssertEqual(String(ashashatWord: fastBall), "[ak'iʔilin]")
    }
}
